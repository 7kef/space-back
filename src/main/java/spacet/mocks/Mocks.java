package spacet.mocks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spacet.dao.FlightRepository;
import spacet.dao.TouristRepository;
import spacet.entities.Flight;
import spacet.entities.Gender;
import spacet.entities.Tourist;

import java.util.*;

@Configuration
@Slf4j
public class Mocks {
    @Bean
    CommandLineRunner initDatabase(FlightRepository flightRepository, TouristRepository touristRepository) {
        List<Flight> listFlight = new ArrayList<>();
        List<Tourist> tourists = new ArrayList<>();
        listFlight.add(new Flight(Long.valueOf("1589970000"), Long.valueOf("1591024515"), 100, 200000));
        listFlight.add(new Flight(Long.valueOf("1589970000"), Long.valueOf("1591024515"), 100, 200000));
        listFlight.add(new Flight(Long.valueOf("1589970000"), Long.valueOf("1591024515"), 100, 200000));
        listFlight.add(new Flight(Long.valueOf("1589970000"), Long.valueOf("1591024515"), 100, 200000));
        listFlight.add(new Flight(Long.valueOf("1589970000"), Long.valueOf("1591024515"), 100, 200000));

        tourists.add(new Tourist("Jan","Kowalski", Gender.MALE, "Poland", "No remarks", Long.valueOf("635472000")));
        tourists.add(new Tourist("Mariusz","Kowalski", Gender.MALE, "Poland", "No remarks", Long.valueOf("635472000")));
        tourists.add(new Tourist("Tomasz","Kowalski", Gender.MALE, "Poland", "No remarks", Long.valueOf("635472000")));
        tourists.add(new Tourist("Wojciech","Kowalski", Gender.MALE, "Poland", "No remarks", Long.valueOf("635472000")));


        listFlight.get(0).addTourist(tourists.get(1));
        tourists.get(1).addFlight(listFlight.get(0));


        return args -> {
            log.info("Preloading " + flightRepository.saveAll(listFlight));
            log.info("Preloading " + touristRepository.saveAll(tourists));
        };
    }
}
