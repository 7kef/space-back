package spacet.services.flights;

import org.springframework.web.bind.annotation.*;
import spacet.dao.FlightRepository;
import spacet.entities.Flight;
import spacet.exceptions.FlightNotFoundException;

import java.util.List;

@RestController
public class FlightController {
    private final FlightRepository repository;

    FlightController(FlightRepository repository){
        this.repository = repository;
    }

    @GetMapping("/flights")
    List<Flight> all(){
        return repository.findAll();
    }

    @PostMapping("/flights/add")
    Flight newFlight(@RequestBody Flight newFlight){
        return repository.save(newFlight);
    }

    @GetMapping("/flights/{id}")
    Flight one(@PathVariable Long id){
        return repository.findById(id)
                .orElseThrow(() -> new FlightNotFoundException(id));
    }

    @PutMapping("/flights/{id}")
    Flight replaceFlight(@RequestBody Flight newFlight, @PathVariable Long id) {

        return repository.findById(id)
                .map(flight -> {
                    flight.setArrivalDate(newFlight.getArrivalDate());
                    flight.setDepartureTime(newFlight.getDepartureTime());
                    flight.setAvailability(newFlight.getAvailability());
                    flight.setTicketPrice(newFlight.getTicketPrice());
                    flight.setTourists(newFlight.getTourists());
                    return repository.save(flight);
                })
                .orElseGet(() -> {
                    newFlight.setId(id);
                    return repository.save(newFlight);
                });
    }

    @DeleteMapping("/flights/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }

}
