package spacet.services.flights;

import org.springframework.web.bind.annotation.*;
import spacet.dao.TouristRepository;
import spacet.entities.Tourist;
import spacet.exceptions.TouristNotFoundException;

import java.util.List;

@RestController
public class TouristController {
    private final TouristRepository repository;

    TouristController(TouristRepository repository){
        this.repository = repository;
    }

    @GetMapping("/tourists")
    List<Tourist> all(){
        return repository.findAll();
    }

    @PostMapping("/tourists/add")
    Tourist newTourist(@RequestBody Tourist newTourist){
        return repository.save(newTourist);
    }

    @GetMapping("/tourists/{id}")
    Tourist one(@PathVariable Long id){
        return repository.findById(id)
                .orElseThrow(() -> new TouristNotFoundException(id));
    }

    @PutMapping("/tourists/{id}")
    Tourist replaceTourist(@RequestBody Tourist newTourist, @PathVariable Long id) {

        return repository.findById(id)
                .map(tourist -> {
                    tourist.setFirstName(newTourist.getFirstName());
                    tourist.setLastName(newTourist.getLastName());
                    tourist.setCountry(newTourist.getCountry());
                    tourist.setDateOfBirth(newTourist.getDateOfBirth());
                    tourist.setGender(newTourist.getGender());
                    tourist.setRemarks(newTourist.getRemarks());
                    tourist.setFlights(newTourist.getFlights());
                    return repository.save(tourist);
                })
                .orElseGet(() -> {
                    newTourist.setId(id);
                    return repository.save(newTourist);
                });
    }

    @DeleteMapping("/tourists/{id}")
    void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }

}
