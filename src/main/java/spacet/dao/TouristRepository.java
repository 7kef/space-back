package spacet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import spacet.entities.Tourist;

public interface TouristRepository extends JpaRepository<Tourist, Long> {
}
