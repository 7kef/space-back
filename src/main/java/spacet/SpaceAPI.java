package spacet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaceAPI {
    public static void main(String[] args) {
        SpringApplication.run(SpaceAPI.class, args);
    }
}
