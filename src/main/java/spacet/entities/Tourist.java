package spacet.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Data
@Entity
public class Tourist {
    private @Id @GeneratedValue Long id;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String country;
    private String remarks;
    private Long dateOfBirth;
    @ManyToMany

    //JSON bidirectional recursion : https://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion
    // to edit JSON use JACKSON: https://www.journaldev.com/2324/jackson-json-java-parser-api-example-tutorial
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id"
        )
    private List<Flight> flights;

    Tourist() {}

    public Tourist(String firstName, String lastName, Gender gender, String country, String remarks, Long dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.country = country;
        this.remarks = remarks;
        this.dateOfBirth = dateOfBirth;
        this.flights = new ArrayList<>();
    }

    public Tourist(String firstName, String lastName, Gender gender, String country, String remarks, Long dateOfBirth, List<Flight> flights) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.country = country;
        this.remarks = remarks;
        this.dateOfBirth = dateOfBirth;
        this.flights = flights;
    }

    public void addFlight(Flight newFlight){
        flights.add(newFlight);
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender=" + gender +
                ", country='" + country + '\'' +
                ", remarks='" + remarks + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
