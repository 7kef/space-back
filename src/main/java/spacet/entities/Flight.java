package spacet.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

@Data
@Entity
public class Flight {

    private @Id @GeneratedValue Long id;
    private Long departureTime;
    private Long arrivalDate;
    private int availability;
    private float ticketPrice;
    @ManyToMany(mappedBy = "flights")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id")
    private List<Tourist> tourists;

    public Flight(){};

    public Flight(Long departureTime, Long arrivalDate, int availability, float ticketPrice) {
        this.departureTime = departureTime;
        this.arrivalDate = arrivalDate;
        this.availability = availability;
        tourists = new ArrayList<Tourist>();
    }

    public Flight(Long departureTime, Long arrivalDate, int availability, float ticketPrice, List<Tourist> tourists) {
        this.departureTime = departureTime;
        this.arrivalDate = arrivalDate;
        this.availability = availability;
        this.ticketPrice = ticketPrice;
        this.tourists = tourists;
    }

    public void addTourist(Tourist newTourist){
        this.tourists.add(newTourist);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", departureTime=" + departureTime +
                ", arrivalDate=" + arrivalDate +
                ", availability=" + availability +
                ", ticketPrice=" + ticketPrice +
                '}';
    }
}
