package spacet.entities;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
