package spacet.exceptions;


public class TouristNotFoundException extends RuntimeException{
    public TouristNotFoundException(String message) {
        super(message);
    }
    public TouristNotFoundException(Long id) {
        super("The tourist with id: "+ id + " can not be found! ");
    }
}
