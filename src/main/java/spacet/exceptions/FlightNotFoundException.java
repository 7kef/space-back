package spacet.exceptions;


public class FlightNotFoundException extends RuntimeException{
    public FlightNotFoundException(String message) {
        super(message);
    }
    public FlightNotFoundException(Long id) {
        super("The flight with id: "+ id + " can not be found! ");
    }
}
